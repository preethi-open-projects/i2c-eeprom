#include<stdio.h>
#include<stdint.h>
#include<stdbool.h>
#include<main.h>
#include<gpio.h>
#include "pin.h"
#include"led.h"
#include"dbgprintf.h"
#include"eeprom.h"


volatile uint32_t user_button_pressed_ts=0;
#define BUTTON_DEBOUNCE_THRESHOLD_MS 200 //milliseconds
static bool _button_press;

//////////////////////////////////////////////////Public functions
void pin_set(void* gpio,uint16_t pin)
{
	if(!gpio)
		return;

	HAL_GPIO_WritePin(gpio, pin, GPIO_PIN_SET);
}

void pin_clear(void* gpio,uint16_t pin)
{
	if(!gpio)
			return;

	HAL_GPIO_WritePin(gpio, pin, GPIO_PIN_RESET);
}

void pin_toggle(void* gpio,uint16_t pin)
{
	if(!gpio)
		return;

	HAL_GPIO_TogglePin(gpio, pin);
}

void pin_run(void)
{
	if(_button_press==true)
	{
		eeprom_dump_data_on_uart();
		_button_press=false;
	}
}

//////////////////////////////////////////////////Private functions


//////////////////////////////////////////////////HAL/SDK functions
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if(GPIO_Pin==GPIO_PIN_13)
	{
		if((HAL_GetTick()-user_button_pressed_ts)>BUTTON_DEBOUNCE_THRESHOLD_MS)
		{
			user_button_pressed_ts=HAL_GetTick();
			led_toggle(DEV_LED_GREEN);
			_button_press=true;
		}
	}
}

