#include<stdio.h>
#include<stdint.h>
#include<string.h>
#include<stdarg.h>
#include"uart.h"

static uint8_t _print_buf[1024];

///////////////////////////////////////////////////Public functions
void dbgprintf(const char *fmt, ...)
{
	va_list args;

	va_start(args,fmt);
	vsnprintf((char *)_print_buf,sizeof(_print_buf),fmt,args);
	va_end(args);

	uart_write_data(_print_buf, (uint16_t)strlen((char *)_print_buf));
}
