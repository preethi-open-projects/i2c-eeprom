
#ifndef LED_H_
#define LED_H_

typedef enum _led_
{
	DEV_LED_GREEN,
	DEV_LED_BLUE,
	DEV_LED_RED,
	DEV_LED_MAX
}led_t;

void led_on(led_t led);
void led_off(led_t led);
void led_toggle(led_t led);

#endif /* LED_H_ */
