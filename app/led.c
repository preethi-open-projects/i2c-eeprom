#include<stdio.h>
#include<main.h>
#include"led.h"
#include"pin.h"

//////////////////////////////////////////////////Public functions
void led_on(led_t led)
{
	switch(led)
	{
	case DEV_LED_GREEN:
		pin_set(LD1_GREEN_GPIO_Port, LD1_GREEN_Pin);
		break;

	case DEV_LED_BLUE:
		pin_set(LD2_BLUE_GPIO_Port,LD2_BLUE_Pin);
		break;

	case DEV_LED_RED:
		pin_set(LD3_RED_GPIO_Port,LD3_RED_Pin);
		break;

	default:
		break;
	}
}

void led_off(led_t led)
{
	switch(led)
	{
	case DEV_LED_GREEN:
		pin_clear(LD1_GREEN_GPIO_Port, LD1_GREEN_Pin);
		break;

	case DEV_LED_BLUE:
		pin_clear(LD2_BLUE_GPIO_Port,LD2_BLUE_Pin);
		break;

	case DEV_LED_RED:
		pin_clear(LD3_RED_GPIO_Port,LD3_RED_Pin);
		break;

	default:
		break;
	}
}

void led_toggle(led_t led)
{
	switch(led)
	{
	case DEV_LED_GREEN:
		pin_toggle(LD1_GREEN_GPIO_Port, LD1_GREEN_Pin);
		break;

	case DEV_LED_BLUE:
		pin_toggle(LD2_BLUE_GPIO_Port,LD2_BLUE_Pin);
		break;

	case DEV_LED_RED:
		pin_toggle(LD3_RED_GPIO_Port,LD3_RED_Pin);
		break;

	default:
		break;
	}

}


//////////////////////////////////////////////////Private functions
