
#ifndef PIN_H_
#define PIN_H_

#include<stdint.h>

void pin_set(void* gpio,uint16_t pin);
void pin_clear(void* gpio,uint16_t pin);
void pin_toggle(void* gpio,uint16_t pin);
void pin_run(void);

#endif /* PIN_H_ */
