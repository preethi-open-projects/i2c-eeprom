#include<stdio.h>
#include<stdint.h>
#include<stdbool.h>
#include"eeprom.h"
#include"i2c.h"
#include"pin.h"
#include"dbgprintf.h"
#include<ctype.h>
#include "checksum.h"
#include"timer.h"

#define I2C_DEV_ADDRESS 			0XA0
#define MAX_BYTES 					32
#define I2C_TIMEOUT_MS  			5000
#define WRITE_INDEX_INFO_MAGIC_NUMBER  0XFA
#define I2C_EEPROM_SIZE 			0X8000
#define WRITE_INDEX_INFO_SIZE 		3
#define WRITE_INDEX_INFO_LOCATION 	I2C_EEPROM_SIZE-WRITE_INDEX_INFO_SIZE-1
#define WRITE_CYCLE_DELAY           5
#define CHECK_SUM_LENGTH			2

static uint8_t _read_buffer[MAX_BYTES];
static uint8_t _write_index_info[WRITE_INDEX_INFO_SIZE];
static bool _initialised;
static uint8_t _dev_addr;
static uint8_t _write_index;

static bool _write(uint16_t mem_addr,uint8_t *data,uint8_t length);
static bool _read(uint16_t mem_addr,uint8_t *data, uint8_t length);
static void _update_write_index(void);
////////////////////////////////////////////////////Public functions

void eeprom_module_init(void)
{
	bool result;

	if(_initialised==true)
		return ;

	_dev_addr=I2C_DEV_ADDRESS;
	result=_read(WRITE_INDEX_INFO_LOCATION,_write_index_info,WRITE_INDEX_INFO_SIZE);
	_write_index=_write_index_info[1];
	if(result==false)
	{
		dbgprintf("Failed to read write index info\r\n");
		dbgprintf("Resetting write index to zero\r\n");
		_write_index=0;
	}
	else
	{
		if(_write_index_info[0]!=WRITE_INDEX_INFO_MAGIC_NUMBER)
		{
			dbgprintf("Magic number mismatch\r\n");
			dbgprintf("Expected %02X but read %02X\r\n",WRITE_INDEX_INFO_MAGIC_NUMBER,
					_write_index_info[0]);
			dbgprintf("Resetting write index to zero\r\n");
			_write_index=0;
		}

	else if(_write_index_info[2]!=checksum_simple(_write_index_info, CHECK_SUM_LENGTH))
		{
			dbgprintf("checksum mismatch\r\n");
			dbgprintf("Expected %02X but read %02X\r\n",_write_index_info,
					_write_index_info[2]);
			dbgprintf("Resetting write index to zero\r\n");
			_write_index=0;
		}
	}
	//pin_set(EEPROM_WP_GPIO_Port,EEPROM_WP_Pin);
	_initialised=true;
}

void eeprom_dump_data_on_uart(void)
{
	bool result;

	if(_initialised== false)
		return ;

	result=_read(0x00, _read_buffer, MAX_BYTES);
	if(result==false)
	{
		dbgprintf("Failed to read eeprom (dev address %02X)\r\n",_dev_addr);
		return;
	}

	result=_read(WRITE_INDEX_INFO_LOCATION,_write_index_info,WRITE_INDEX_INFO_SIZE);
	if(result==false)
	{
		dbgprintf("Failed to read eeprom (dev address %02X)\r\n",_dev_addr);
		return;
	}



//	HAL_StatusTypeDef return_val;
//
//
//	return_val=HAL_I2C_Mem_Read(&hi2c1, _dev_addr, 0X0, I2C_MEMADD_SIZE_16BIT,
//			_read_buffer, MAX_BYTES, I2C_TIMEOUT_MS);
//
//	if(return_val!=HAL_OK)
//	{
//		dbgprintf("Failed to read eeprom (dev address %02X)\r\n",_dev_addr);
//		return;
//	}


	for(uint16_t i=0 ; i<MAX_BYTES ; i++)
	{
		if(isprint(_read_buffer[i])!=0)
			dbgprintf(" %c",_read_buffer[i]);
		else
			dbgprintf(" %02X",_read_buffer[i]);
	}

	dbgprintf("[%02X,%02X,%02X]\r\n",_write_index_info[0],_write_index_info[1],_write_index_info[2]);

	dbgprintf("\r\n");
}


bool eeprom_write(uint8_t data_byte)
{
	if(_initialised==false)
		return false;

	bool result;

	result=_write(_write_index,&data_byte,1);
	if(result==false)
	{
		dbgprintf("Failed to write eeprom (dev address %02X)\r\n",_dev_addr);
		return false;
	}

	_write_index=((_write_index+1) % MAX_BYTES);
	_update_write_index();

	return true;
}

//////////////////////////////////////////////////////Private functions
static bool _write(uint16_t mem_addr,uint8_t *data,uint8_t length)
{
	if(!data)
		return false;

	HAL_StatusTypeDef return_val;

	return_val=HAL_I2C_Mem_Write(&hi2c1, _dev_addr,mem_addr, I2C_MEMADD_SIZE_16BIT,
				data, length, I2C_TIMEOUT_MS);
	timer_delay_ms(WRITE_CYCLE_DELAY);
	if(return_val!=HAL_OK)
		return false;

	return true;
}

static bool _read(uint16_t mem_addr,uint8_t *data, uint8_t length)
{
	if(!data)
		return false;

	HAL_StatusTypeDef return_val;

	return_val=HAL_I2C_Mem_Read(&hi2c1, _dev_addr,mem_addr, I2C_MEMADD_SIZE_16BIT,
				data, length, I2C_TIMEOUT_MS);

	if(return_val!=HAL_OK)
		return false;

	return true;
}

static void _update_write_index(void)
{
	bool result;

	_write_index_info[0]=WRITE_INDEX_INFO_MAGIC_NUMBER;
	_write_index_info[1]=_write_index;
	_write_index_info[2]=checksum_simple(_write_index_info, CHECK_SUM_LENGTH);

	result=_write(WRITE_INDEX_INFO_LOCATION, _write_index_info, WRITE_INDEX_INFO_SIZE);
	if(result==false)
		dbgprintf("Failed to update write index information in EEPROM\r\n");

}
