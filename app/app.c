#include<stdio.h>
#include "app.h"
#include"main.h"
#include"gpio.h"
#include<string.h>
#include<stdbool.h>
#include"pin.h"
#include"led.h"
#include"timer.h"
#include "dbgprintf.h"
#include"uart.h"
#include"eeprom.h"
#include"i2c.h"


#define LED_BLINK_DELAY_MS 4000


void app_start(void)
{
	uart_module_init();

	led_on(DEV_LED_GREEN);
	led_on(DEV_LED_BLUE);
	led_on(DEV_LED_RED);

	printf("Hello World. printf() is cool!!!\r\n");
	dbgprintf("This is dbgprintf()\r\n");

	timer_delay_ms(LED_BLINK_DELAY_MS);

	eeprom_module_init();

	while(true)
	{
		uart_run();
		pin_run();
	}
}
