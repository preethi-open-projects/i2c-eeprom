#include<stdio.h>
#include<stdint.h>
#include<stdbool.h>
#include "timer.h"
#include<main.h>



//////////////////////////////////////////////////Public functions
void timer_delay_ms(uint32_t delay_ms)
{
	uint32_t tick_start = HAL_GetTick();

	while((HAL_GetTick()-tick_start) < delay_ms)
	{

	}
}

void timer_non_blocking_delay_start(non_blocking_delay_t *delay)
{
	if(!delay)
		return;

	delay->end_ts = HAL_GetTick()+delay->delay_ms;
}

bool timer_is_non_blocking_delay_complete(non_blocking_delay_t *delay)
{
	if(!delay)
		return false;

	if(HAL_GetTick()>=delay->end_ts)
		return true;

	return false;
}

uint32_t get_current_ts(void)
{
	return  HAL_GetTick();
}

//////////////////////////////////////////////////Private functions


