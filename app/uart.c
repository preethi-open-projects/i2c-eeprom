#include<stdio.h>
#include<stdint.h>
#include<stdbool.h>
#include"usart.h"
#include"uart.h"
#include"led.h"
#include"dbgprintf.h"
#include"eeprom.h"

#define UART_TRANSMIT_TIMEOUT_MS 1000

typedef enum __state_
{
	ST_SETUP_READ,
	ST_WAIT_FOR_NEW_DATA,
	ST_PROCESS_DATA,
}state_t;

static state_t _state;
static bool _initialised;
static uint8_t _data_from_uart;
static bool _data_available;

static void _process_data(uint8_t data_byte);
//////////////////////////////////////////////////Public functions

void uart_module_init(void)
{
	if(_initialised==true)
		return;

	_state=ST_SETUP_READ;
	_initialised=true;
}
void uart_write_data(uint8_t* data ,uint16_t len)
{
	if(!data)
			return;

	HAL_UART_Transmit(&huart1, data , len , UART_TRANSMIT_TIMEOUT_MS);
}

void uart_run(void)
{
	HAL_StatusTypeDef result;

	if(_initialised==false)
		return;

	switch(_state)
	{
	case ST_SETUP_READ:
		_data_available=false;
		result=HAL_UART_Receive_IT(&huart1, &_data_from_uart, 1);
		if(result==HAL_OK)
			_state=ST_WAIT_FOR_NEW_DATA;
		break;

	case ST_WAIT_FOR_NEW_DATA:
		if(_data_available==true)
			_state=ST_PROCESS_DATA;
		break;

	case ST_PROCESS_DATA:
		_process_data(_data_from_uart);
		_state=ST_SETUP_READ;
		break;

	default:
		break;
	}
}


//////////////////////////////////////////////////Private functions
static void _process_data(uint8_t data_byte)
{
	if((data_byte>='A' && data_byte<='Z') || (data_byte>='a' && data_byte<='z'))
	{
		if(eeprom_write(data_byte)==true)
		{
			dbgprintf("\r\nWrote letter %d\r\n",data_byte);
		}
		else
		{
			dbgprintf("\r\nFailed to write letter %d\r\n",data_byte);
		}
	}

	else if(data_byte=='/')
	{
		eeprom_dump_data_on_uart();
	}

	else
	{
		dbgprintf("\r\nUnsupported format %c [%02X]\r\n",data_byte,data_byte);
	}


//	switch(data_byte)
//	{
//	case 'A':
//		dbgprintf("\r\nReceived A\r\n");
//		break;
//	case 'B':
//		dbgprintf("\r\nReceived B\r\n");
//		break;
//
//	case 'g':
//	case 'G':
//		led_toggle(DEV_LED_GREEN);
//		break;
//
//	case '/':
//		eeprom_dump_data_on_uart();
//		break;
//
//	default:
//		dbgprintf("\r\nUnsupported format %c [%02X]\r\n",data_byte,data_byte);
//		break;
//	}
}
////////////////////////////////////////////////////////
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *uart)
{
	if(uart==&huart1)
		_data_available=true;
}

int __io_putstr(char *data,int len)
{
	uart_write_data((uint8_t*)data, len);
	return len;
}
