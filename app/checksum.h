
#ifndef CHECKSUM_H_
#define CHECKSUM_H_

static inline unsigned char checksum_simple(unsigned char *data,unsigned char length)
{
	unsigned char count;
	unsigned int sum=0;

	for (count=0;count<length;count++)
		sum=sum+data[count];

	sum=-sum;

	return(sum & 0xFF);
}


#endif /* CHECKSUM_H_ */
