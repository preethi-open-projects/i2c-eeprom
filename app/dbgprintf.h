
#ifndef DBGPRINTF_H_
#define DBGPRINTF_H_

void dbgprintf(const char *fmt, ...);

#endif /* DBGPRINTF_H_ */
